**Game Mechanics**

# General Gameplay

-    Tutorial is 0.5 difficulty
-    Amateur is 1 difficulty
-    Intermediate is 1.5 difficulty
-    Professional is 2 difficulty

# Sanity
**Game internally uses insanity, which is then displayed as sanity in the truck as (100 - insanity) + Random.Range(-2, 3)**


| Small (Map) | Medium (Map) | Large (Map) | 
| ------ | ------ | ------ |
| Normal drop rate: 0.12 | Normal drop rate: 0.08 | Normal drop rate: 0.05 |
| Setup drop rate: 0.09 | Setup drop rate: 0.05 | Setup drop rate: 0.03 |

- **Insanity** 
  - Capped at 50 during setup phase
  - Uncapped to 100 during regular play
  - Increases every frame by ((deltaTime * setup/normal drop rate) * difficulty rate (below))
  - Insanity does not increase when you are in light or are outside the house
  - Light status updates every 2 seconds
  - Insanity level is synced to other players every 5 seconds
  - Being within 3m of a Jinn instantly decreases your insanity by 25%
  - Entering a recognised phrase into a Ouija Board has a 1-in-3 chance of increasing your insanity by 40%
  - Sanity pills decrease your insanity by 40%
  - Poltergeists increase your insanity by double the number of props they are allowed to move
  
- If the ghost is visible, or is in hunting phase, and is within 10m of the player, the player's insanity raises by deltaTime (time since last frame) * sanity drain strength

| Poltergeist | Banshee | Jinn | Spirit | Wraith | Phantom | Mare | Revenant | Shade | Demon | Yurei | Oni |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 0.2 | 0.2 | 0.2 | 0.2 | 0.2 | 0.4 | 0.2 | 0.2 | 0.2 | 0.2 | 0.2 | 0.2 |
| Default | Default | Default | Default | Default | **Custom** | Default | Default | Default | Default | Default | Default |

- **Sanity effects**
  - The game will attempt to spawn ghosts at a window when the player is inside the house at a random interval between 10s and 30s if the player's insanity is above 50, otherwise at a random interval between 10s and 20s.
  - This ghost will start far away from the window and dash towards the window until it is 0.2m away, then disappear

# Evidence

- **Dirty Water**
   - Ghosts will use a tap and spawn dirty water as soon as they enter a bathroom
   - Ghosts leave a Ghost Interaction EMF (Level 2) on the sink
- **Freezing Temperatures**
   - Ghosts that have freezing temperatures as an attribute will make the room they are in go down to -10C (14F), while all other ghosts make the room drop to 5C (41F). However, your thermometer has a +-2C randomness, so as soon as you see your thermometer go below 3C or 37.4F, or if you see cold puffs when you are in a room, you can tick off Freezing Temperatures.
- **Ghost Orbs**
   - Ghost orbs only spawn in the ghost's favourite room, and can be seen in the doorway to that room from a hallway. This favourite room is designated on ghost spawn
   - Ghost orbs can be seen before entering the house if a fixed camera is pointing at the door/hallway to their favourite room.
- **Ghost Writing**
   - Ghosts have a 5-in-12 chance of doing random interactions such as opening doors, throwing props, and writing in ghost books.
   - The ghost book does not appear to need to be in the ghost's room. Yet to be confirmed.
- **Spirit Box**
  - Ghosts can only respond once every 10 seconds
"Nothing detected" means the game recognised your phrase, but it triggered a fail check. However, fail checks and answers do not seem to be mutually exclusive.
  - Ghosts that do not use spirit box will never respond
  - Ghost responses only work if you are using Local Push to Talk (if you have push to talk enabled) or have a VR headset on. If you use Voice Activation, the ghost will always hear you.
  - Ghosts will not respond if you are more than 3m away from them, or are on a different floor
  - Ghosts that only respond to one person are shy
  - Ghosts will not respond if the fusebox is on and a light switch in your current room is on
  - Ghosts are more likely to respond to the Spirit Box if you have said their name in isolation (i.e. anger them with a phrase) recently
  - Ghosts can respond with either location answers, difficulty answers, or age answers
  - Different location sounds are played depending on whether you are at least 4m away from the ghost
``
- **Difficulty questions:**
    - What do you want?
    - Why are you here?
    - Do you want to hurt us?
    - Are you angry?
    - Do you want us here?
    - Shall we leave?
    - Should we leave?
    - Do you want us to leave?
    - What should we do?
    - Can we help?
    - Are you friendly?
    - What are you?
- **Location questions:**
    - Where are you?
    - Are you close?
    - Can you show yourself?
    - Give us a sign
    - Let us know you are here
    - Show yourself
    - Can you talk?
    - Speak to us
    - Are you here?
    - Are you with us?
    - Anybody with us?
    - Is anyone here?
    - Anyone in the room?
    - Anyone here?
    - Is there a spirit here?
    - Is there a Ghost here?
    - What is your location?
- **Gender questions:**
    - Are you a girl?
    - Are you a boy?
    - Are you male?
    - Are you female?
    - Who are you?
    - What are you?
    - Who is this?
    - Who are we talking to?
    - Who am I talking to?
    - Hello?
    - What is your name?
    - Can you give me your name?
    - What is your gender?
    - What gender?
    - Are you male or female?
    - Are you a man?
    - Are you a woman?
- **Age questions:**
    - How old are you?
    - How young are you?
    - What is your age?
    - When were you born?
    - Are you a child?
    - Are you old?
    - Are you young?
- **Generic VOIP recognition**
   - Ghosts can react with your voice all throughout the game, even when you are not holding down a VOIP key
   - You must be in the ghost room to trigger a response
   - You must be in a room for more than 2 seconds for phrases to be recognised
   - Shy ghosts will not respond to you if there are multiple people in the room
   - Ghosts can react to their name at any time, even when you are not holding down a VOIP key, with a 20 second delay in between reactions
   - Saying the ghost's name makes the ghost more likely to respond to the Spirit Box
   - Saying just the ghost's first name is sufficient
   - A generic ghost reaction will add a random value between 10 and 25 to their activity multiplier
   - Generic reactions have a 1-in-200 chance of the ghost turning the fusebox off
   - Generic reactions have a 1-in-8 chance of interacting with a random prop
   - Generic reactions have a 1-in-8 chance of interacting with a random door    
- Ghosts have random ability values set at the start of an idle phase, based on difficulty.
   - Amateur: 100
   - Intermediate: 115
   - Professional: 130
- Certain ghosts have their own multiplier that change depending on in-game events:
   - Oni multiplier: 30 if the ghost is an Oni and there are people in the room
   - Wraith multiplier: 50 if ghost is a Wraith and they have walked in salt
- **Hunting multiplier**
   - Starts at 0
   - Does not increase steadily over time
   - Decreases by 10 if ghost type is a Mare and a light switch in their current room is on
   - Increases by 10 if ghost type is a Mare and no lights in their current room are on, or there are no light switches in their current room
   - Increases by 15 if the ghost type is a Demon
- **Ghost activity level**
   - Starts at 0
   - Increases by 10 to 25 on phrase recognition
   - Increases by 20-30 if a smudge stick is used within 6m of it
   - Decays by deltaTime (time since last frame) / 2 every frame, capped at 0
- If a random number from 0 to <random ability value> (based on difficulty) is greater than or equal to their current activity level, including all ghost-specific multipliers, and an additional 15% if playing solo, they have a chance of interacting with objects, or going to their favourite room, appear, use the fusebox, etc.
   - This means that a higher Ghost activity level, Oni/Wraith multiplier, and playing solo decreases the chance of the ghost interacting with the house
- Ghosts can throw any prop around the house. A prop moving does not indicate the ghost being nearby. Poltergeists throw objects with a random force between ((-5, 5), (-2.5, 2.5), (-3, 3)), while other ghosts throw them with a random force between ((-2.5, 2.5), (-2, 2), (-2.5, 2.5))